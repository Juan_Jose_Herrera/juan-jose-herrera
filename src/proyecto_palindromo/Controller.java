package proyecto_palindromo;

import javax.swing.JOptionPane;

/**
 *
 * @author Juan Jose Herrera
 */
public class Controller {

    public static void main(String[] args) {

        String texto = JOptionPane.showInputDialog("Por favor ingrese el texto");
        String[] arreglo = texto.split(" ");
//        PalabraPalindroma palabraPalindroma = new PalabraPalindroma();
        PalabraPalindroma palabraPalindroma = new PalabraPalindroma();
        for (int j = 0; j < arreglo.length; j++) {
            if (arreglo[j].length() > 1) {
//                palabraPalindroma.verificar(arreglo[j], 0, arreglo[j].length() - 1);
//                      System.out.println(arreglo[j]);
                char[] herramientaConvertir = arreglo[j].toCharArray();
                palabraPalindroma.convertir(arreglo[j], 0, 0, 0, herramientaConvertir);

            }
        }
    }
}
